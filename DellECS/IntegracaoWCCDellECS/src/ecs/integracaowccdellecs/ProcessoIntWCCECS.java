package ecs.integracaowccdellecs;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.FileOutputStream;
import java.io.IOException;

import java.io.InputStream;

import java.io.OutputStream;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import java.util.Map;
import java.util.TimeZone;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import ecs.utilities.Header;
import ecs.utilities.S3AuthorizationV4;

import java.sql.Array;

import java.util.Properties;

public class ProcessoIntWCCECS {

    /*
    * Oracle Consulting
    * Versão 1.0 16/07/2019
    * Ultimas Alteracoes:
    * 1.0
    * Versao inicial
    */

    Properties metadataProperties = null;
    Properties storageRuleProperties = null;
    
    private static final boolean debug = true;


    public ProcessoIntWCCECS() {
        super();
    }

    private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
   
    private static Connection getDBConnection(String db, String dbuser, String dbpass) throws SQLException {
	   
        Connection dbConnection = null;
        dbConnection=DriverManager.getConnection(db, dbuser, dbpass);
        dbConnection.setAutoCommit(false);
        return dbConnection;
        
    }


    private Properties readProperties ( String resFile) {

        InputStream res = null;
        try {
            res = getClass().getResourceAsStream(resFile);
            
            if (res == null) {
                if (debug) System.out.println("Erro a ler ficheiro propriedades:"+resFile);
                return null;

            } else {
                Properties prop = new Properties();
                prop.load(res);
                return prop;
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (debug) System.out.println("Erro a ler ficheiro propriedades:"+resFile);
            return null;
        }
    }

    public static void main(String[] args){
        
        Connection dbConnection = null;
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        DateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        SimpleDateFormat formatoData = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatoHora = new SimpleDateFormat("HHmmss");

        Date data = new Date();
        
        System.out.println("***** Processo iniciado:"+dateFormat1.format(data));
    


        try{
            Exception err = new Exception ();
            ProcessoIntWCCECS  proc = new ProcessoIntWCCECS();
            proc.setMetadataProperties(proc.readProperties("/metadata.properties")); 
            proc.setStorageRuleProperties(proc.readProperties("/parametros.properties")); 

        
            //System.out.println(proc.metadataProperties.toString());
            //System.out.println(proc.storageRuleProperties.toString());
     
            // obter
            String stRule = proc.storageRuleProperties.getProperty("storageRule");
            String dbUser = proc.storageRuleProperties.getProperty("dbUser");
            String dbPass = proc.storageRuleProperties.getProperty("dbPass");
            String dbHost = proc.storageRuleProperties.getProperty("dbHost");
            String dbPort = proc.storageRuleProperties.getProperty("dbPort");
            String dbService = proc.storageRuleProperties.getProperty("dbService");
            String dbConn = "jdbc:oracle:thin:@//"+dbHost+":"+dbPort+"/"+dbService;
            String storageHost = proc.storageRuleProperties.getProperty("storageHost");
            String storagePort = proc.storageRuleProperties.getProperty("storagePort");
            String docAccounts = proc.storageRuleProperties.getProperty("docAccounts");
            
            int erro = 0;
            if (stRule == null || stRule.equals("")) {System.out.println("**** Erro: storageRule não definido no ficheiro de parâmetros"); erro ++;}
            if (dbUser == null || dbUser.equals("")) {System.out.println("**** Erro: dbUser não definido no ficheiro de parâmetros"); erro ++;}
            if (dbPass == null || dbPass.equals("")) {System.out.println("**** Erro: dbPass não definido no ficheiro de parâmetros"); erro ++;}
            if (dbHost == null || dbHost.equals("")) {System.out.println("**** Erro: dbhost não definido no ficheiro de parâmetros"); erro ++;}
            if (dbPort == null || dbPort.equals("")) {System.out.println("**** Erro: dbPort não definido no ficheiro de parâmetros"); erro ++;}
            if (dbService == null || dbService.equals("")) {System.out.println("**** Erro: dbService não definidas no ficheiro de parâmetros"); erro ++;}
            if (storageHost == null || storageHost.equals("")) {System.out.println("**** Erro: storageHost não definido no ficheiro de parâmetros"); erro ++;}
            if (storagePort == null || storagePort.equals("")) {System.out.println("**** Erro: storagePort não definidas no ficheiro de parâmetros"); erro ++;}
            

            String[] stAccountArray = null;
            String stAccountString = null;
            if (docAccounts == null || docAccounts.equals("") ||docAccounts.equals("TODAS")) docAccounts = "TODAS";
            else {

                try{                
                    stAccountArray = docAccounts.split(",");
                    for (String rule: stAccountArray) { 
                        if (stAccountString==null) stAccountString = "?";
                        else stAccountString = stAccountString + ",?";
                        if (debug) System.out.println("Accounts:"+rule );
                        
                    } 
                    if (debug) System.out.println("stAccountString:"+stAccountString);
                } catch (Exception e) {
                        erro ++;
                        e.printStackTrace();                    
                }
            }


            String[] stRuleArray = null;
            String stRuleString = null;
            try{                
                stRuleArray = stRule.split(",");
                for (String rule: stRuleArray) {                    
                    if (stRuleString==null) stRuleString = "?";
                    else stRuleString = stRuleString + ",?";
                    if (debug) System.out.println("Storage Rules:"+rule );
                    
                } 
                if (debug) System.out.println("ruleString:"+stRuleString);
            } catch (Exception e) {
                    erro ++;
                    e.printStackTrace();                    
            }

        
    
    
            try{
                if (debug) System.out.println("Conexão: " + dbConn+"  User:"+ dbUser);
                String querie = "select user from dual";
                dbConnection = getDBConnection(dbConn, dbUser, dbPass);
                PreparedStatement stQuerie = dbConnection.prepareStatement(querie);
                ResultSet resultSetCount = stQuerie.executeQuery();
                
                while(resultSetCount.next()) {
                    if (debug) System.out.println( resultSetCount.getString("user"));
                }
                stQuerie.close();
    
                
            } catch (SQLException sql ) {
                erro ++;
                sql.printStackTrace();            
                System.out.println("**** Erro ao efetuar ligação à base de dados");
            }  catch (Exception sql2) {
                erro ++;
                sql2.printStackTrace();
                System.out.println("**** Erro ao efetuar ligação à base de dados");
                    
            }
        
            
            try{
                // Lê e valida Metadata Properties; dá erro se a metadata n estiver no formato metadatax=meta1,meta2
                int metasize = proc.getMetadataProperties().size();
                System.out.println("metadatasize:"+metasize);
                for (int i=1; i<=metasize; i++) {
                    String meta = proc.metadataProperties.getProperty("metadata"+i);
                    //System.out.println("key: "+i + " keyValue:"+meta);
                    String[] metaArray = meta.split(",");
                    System.out.println(metaArray[0] + ">"+metaArray[1]);
                    
                }
            } catch (Exception meta) {
                erro ++;
                meta.printStackTrace();
            }
            
                        
            // validar ligação ao object Storage
            try{
                // Testar processo Authorization AWS V4                
                S3AuthorizationV4  auth= new S3AuthorizationV4();
                Header header = new Header ();
                List<Header> headers = new ArrayList<Header>(); 

                dateFormat2.setTimeZone(TimeZone.getTimeZone("UTC"));//server timezone
                String dateFormatS3 = dateFormat2.format(new Date());



                auth.setHost(storageHost);
                auth.setPort(storagePort);
                auth.setHttpMethod("HEAD");
                auth.setCannonicalUri("/bkpciprpcdev"); //+ "/"+ "iss/@pcirpc/@coimbra/3/X999/64935.pdf"); // "ALL.csv"); //iss/@pcirpc/@coimbra/3/X999/64935.pdf"); //   /iss/@pcirpc/@coimbra/3/X999/64935.pdf");
                auth.setAccessKey("user_pciprpc_dev");
                auth.setSecretKey("DGbw5uHDWbLclMQSwetB8zc6e2j8wqu5kw9LLC3z");
                auth.setRegionStandard("Standard");
                auth.setServiceNameS3("s3");
                auth.setAws4_request("aws4_request");
                auth.setData(dateFormatS3); //"20190711T183105Z");
                auth.setDataTrunc(formatoData.format(data)); //20190711
            
                header = new Header();
                header.setKey("Host");
                header.setValue("");
                headers.add(header);
                
                header = new Header();
                header.setKey("X-Amz-Content-Sha256");
                header.setValue("");
                headers.add(header);
                
                header = new Header();
                header.setKey("X-Amz-Date");
                header.setValue("");
                headers.add(header);
            
                header = new Header();
                header.setKey("Authorization");
                header.setValue("");
                headers.add(header);
                // Inicialização headers
                auth.setHeaders(headers);
                // debug com recurso a System.out.println 
                auth.setDebug(false);
                // inicialização debug do processo. em caso de sucesso, o valor manter-se-á.
                // em caso de erro, este atributo terá pistas sobre os erros que ocorreram durante o processo
                auth.setDebugProcesso("");
            
                String authorization = auth.getAuthorization();
                
                if (authorization == null || authorization.equals("")) {
                    erro ++;
                    System.out.println("**** Erro: Erro na obtenção da Authorization para autenticação S3 Rest Api");
                    System.out.println("**** Erro: "+auth.getDebugProcesso());
                }
                else {
                        if (debug)   System.out.println("A Authorization S3 Rest Api é:"+authorization);
                        //System.out.println(this.getHeaders());
                        Iterator<Header> headerIterator = auth.getHeaders().iterator();
                        
                        // Validar que os seguintes headers obrigatórios estão presentes
                        
                        while (headerIterator.hasNext()) {
                            Header h = headerIterator.next();
                            if (debug) System.out.println(h.getKey()+ ":"+ h.getValue());
                        }
                }
                
                /**********
                 * FALTA TESTAR Chamada à Rest API S3 
                 * *********/
                
                
            } catch (Exception au) {
                erro ++;
                au.printStackTrace();
            }
            
            
            erro = 0;
            if (erro > 0) System.out.println("***** Processo cancelado por ocorrência de erros de validação dos parâmetros iniciais.");
            else {
                
                
                String qAccounts = new String ();
                if (docAccounts.equals("TODAS")) qAccounts = "";
                else qAccounts = "and r.ddocaccount in ("+ stAccountString +") \n";
                
                // Executar processo
                String processQuery = 
                    "select r.did, r.ddocname, r.ddoctitle, to_char(r.dreleasedate,'dd-mm-yyyy hh24:mi:ss') dreleasedate, r.ddocauthor, r.ddocaccount, r.dwebextension, d.doriginalname, d.dfilesize, m.* \n"+
                    "from documents d \n"+
                    ", revisions r \n"+
                    ", docmeta m \n"+
                    "where d.did = r.did \n"+
                    "and d.did = m.did \n"+
                    qAccounts +
                    "and m.xstoragerule in ("+ stRuleString +") \n"+
                    "and r.dstatus = 'RELEASED' \n"+
                    "and d.disprimary = 1 \n"+
                    "and not exists ( select 'x' \n"+
                    "                 from II_integracao_wcc_ecs_log i \n"+
                    "                 where r.did = i.did \n"+
                    "                 and i.ESTADO = 'OK' \n"+
                    "                 and nvl(i.data,r.dreleasedate) >= r.dreleasedate \n"+
                    "                 ) "; 
                
                PreparedStatement stQuerie = dbConnection.prepareStatement(processQuery);
                
                int i = 1;
                // tratar accounts
                if (!qAccounts.equals("")) {
                    for (String acc: stAccountArray) { 
                        stQuerie.setString(i, acc);
                        i++;
                    }
                }
                // tratar Storage Rules
                if (!qAccounts.equals("")) {
                    for (String rule: stRuleArray) { 
                        stQuerie.setString(i, rule);
                        i++;
                    }
                }
                System.out.println("i="+i);

                ResultSet resultSetCount = stQuerie.executeQuery();

                while(resultSetCount.next()) {
                    if (debug) System.out.println( resultSetCount.getInt("did"));
                }
                stQuerie.close();

                
                if (debug) System.out.println("Querie final: \n"+processQuery);

                data = new Date();
                System.out.println("***** Processo concluído com sucesso às "+dateFormat1.format(data));
            }
//        } catch (SQLException sql ) {
//                sql.printStackTrace();
//            date = new Date();
//            System.out.println("***** Processo concluído erro SQLException às "+dateFormat.format(date));
        } catch (Exception exc) {
                exc.printStackTrace();
                data = new Date();
                System.out.println("***** Processo concluído erro Exception às "+dateFormat1.format(data));
        } finally {
                // close db
                try{ dbConnection.close();}catch (Exception e){}
        }                        

        
    }

    public void setMetadataProperties(Properties metadataProperties) {
        this.metadataProperties = metadataProperties;
    }

    public Properties getMetadataProperties() {
        return metadataProperties;
    }

    public void setStorageRuleProperties(Properties storageRuleProperties) {
        this.storageRuleProperties = storageRuleProperties;
    }

    public Properties getStorageRuleProperties() {
        return storageRuleProperties;
    }
}
