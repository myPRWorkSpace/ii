package ecs.utilities;

public class Header {
    /*
    * Oracle Consulting
    * Versão 1.0 14/07/2019
    * Ultimas Alteracoes:
    * 1.0
    * Versao inicial
    */


    String key;
    String value;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    public Header() {
        super();
    }
}
