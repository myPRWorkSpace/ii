package ecs.utilities;


import java.io.UnsupportedEncodingException;

import java.net.URISyntaxException;

import java.net.URLEncoder;

import java.nio.charset.StandardCharsets;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

//import com.sun.org.apache.xml.internal.security.utils.Base64;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.Iterator;
import java.util.List;

import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class S3AuthorizationV4 {

    /*
    * Oracle Consulting
    * Versão 1.0 14/07/2019
    * Ultimas Alteracoes:
    * 1.0
    * Versao inicial
    */

    String host;
    String port;
    String httpMethod;
    String CannonicalUri;
    String accessKey;
    String secretKey;
    String regionStandard;
    String serviceNameS3 ;
    String aws4_request;
    String data;
    String dataTrunc;
    Boolean debug;
    String debugProcesso;
    
    Endpoint endpoint;
    List<Header> headers;
    String signedHeaders;
    
    private static final String HMAC_SHA256 = "HmacSHA256";
    private static final String emptyPayload = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
    private static final String AWS4HMACSHA256 = "AWS4-HMAC-SHA256";



    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setCannonicalUri(String CannonicalUri) {
        this.CannonicalUri = CannonicalUri;
    }

    public String getCannonicalUri() {
        return CannonicalUri;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setRegionStandard(String regionStandard) {
        this.regionStandard = regionStandard;
    }

    public String getRegionStandard() {
        return regionStandard;
    }

    public void setServiceNameS3(String serviceNameS3) {
        this.serviceNameS3 = serviceNameS3;
    }

    public String getServiceNameS3() {
        return serviceNameS3;
    }

    public void setAws4_request(String aws4_request) {
        this.aws4_request = aws4_request;
    }

    public String getAws4_request() {
        return aws4_request;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setDataTrunc(String dataTrunc) {
        this.dataTrunc = dataTrunc;
    }

    public String getDataTrunc() {
        return dataTrunc;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    public Boolean getDebug() {
        return debug;
    }

    public void setDebugProcesso(String debugProcesso) {
        this.debugProcesso = debugProcesso;
    }

    public String getDebugProcesso() {
        return debugProcesso;
    }

    public void setHeaders(List<Header> headers) {
        this.headers = headers;
    }

    public List<Header> getHeaders() {
        return headers;
    }

    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public void setSignedHeaders(String signedHeaders) {
        this.signedHeaders = signedHeaders;
    }

    public String getSignedHeaders() {
        return signedHeaders;
    }


    public S3AuthorizationV4() {
        super();
    }

    
    static byte[] HmacSHA256(String data, byte[] key) throws Exception {
        try{
        
            String algorithm="HmacSHA256";
            Mac mac = Mac.getInstance(algorithm);
            mac.init(new SecretKeySpec(key, algorithm));
            return mac.doFinal(data.getBytes("UTF-8"));
    
        }
        catch (Exception e) {
            e.printStackTrace(); 
            return "".getBytes();   
        }
    }

    
    // este metodo foi validado com os valores de https://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html
    static byte[] getSignatureKey(String key, String dateStamp, String regionStandard, String serviceNameS3, String aws4_request, boolean debug) {
        try{
        
            byte[] kSecret = ("AWS4" + key).getBytes("UTF-8");
            if (debug)    System.out.println("getSignatureKey: keySecret:"+bytesToHex(kSecret,debug));
            byte[] kDate = HmacSHA256(dateStamp, kSecret);
            if (debug)    System.out.println("getSignatureKey: kDate:"+bytesToHex(kDate,debug));
            byte[] kRegion = HmacSHA256(regionStandard, kDate);
            if (debug)    System.out.println("getSignatureKey: kRegion:"+bytesToHex(kRegion,debug));
            byte[] kService = HmacSHA256(serviceNameS3, kRegion);
            if (debug)    System.out.println("getSignatureKey: kService:"+bytesToHex(kService,debug));
            byte[] kSigning = HmacSHA256(aws4_request, kService);
            if (debug)    System.out.println("getSignatureKey: kSigning:"+bytesToHex(kSigning,debug));
        
            return kSigning;
        }
        catch (Exception e) {
            e.printStackTrace(); 
            return "".getBytes();   
        }       
    }


    private static String bytesToHex(byte[] hashInBytes, boolean debug) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashInBytes.length; i++) {
            sb.append(Integer.toString((hashInBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        if (debug) System.out.println("bytestohex: "+hashInBytes.toString() + "==>"+sb.toString());
        return sb.toString();
    }

    private static String getTimeStamp() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));//server timezone
        return dateFormat.format(new Date());
    }
    
    public void setHeadersAuthorization ( String authorization) throws Exception {
        
        Exception e = new Exception ();
        
        // Preencher headers
        try{
            
            //System.out.println(this.getHeaders());
            Iterator<Header> headerIterator = this.getHeaders().iterator();
            
            // Validar que os seguintes headers obrigatórios estão presentes
            this.setSignedHeaders("host;x-amz-content-sha256;x-amz-date");
            while (headerIterator.hasNext()) {
                Header h = headerIterator.next();
                if (h.getKey().equals("Host")) h.setValue(this.getHost()+":"+this.getPort()); 
                if (h.getKey().equals("X-Amz-Content-Sha256")) h.setValue(emptyPayload); 
                if (h.getKey().equals("X-Amz-Date")) h.setValue(this.getData());
                if (h.getKey().equals("Authorization")) {
                    // Sample full Authorization key
                    // AWS4-HMAC-SHA256 Credential=user_pciprpc_dev/20190711/Standard/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=a70531e05b46539d8fd1411156cb19975a68b90cd89e00dc27bf97e32ec1ed81
                    h.setValue(AWS4HMACSHA256 + " "+
                               "Credential="+this.getAccessKey()+"/"+this.getDataTrunc()+"/"+this.getRegionStandard()+"/"+this.getServiceNameS3()+"/"+this.getAws4_request()+", "+
                               "SignedHeaders="+this.getSignedHeaders() + ", "+
                               "Signature="+authorization);
                }
            }
            

        }
        catch (Exception e1) {
            e1.printStackTrace();
            this.setDebugProcesso("Headers são obrigatórios. Pelo menos:Host; X-Amz-Content-Sha256; X-Amz-Date e Authorization");
            throw e;
        }        
        
    }
    
    public void processValidation () throws Exception {
        
        Exception e = new Exception ();
        if (this.getHost()== null || this.getHost().equals("")) {this.setDebugProcesso("Host é obrigatório"); throw e;}  
        if (this.getPort()== null || this.getPort().equals("")) {this.setDebugProcesso("Port é obrigatório"); throw e;}  
        if (this.getHttpMethod()== null || this.getHttpMethod().equals("")) {this.setDebugProcesso("HttpMethod é obrigatório e com valor GET ou PUT ou HEAD"); throw e;}
        else {
            if (this.getHttpMethod().equals("GET") || this.getHttpMethod().equals("PUT") || this.getHttpMethod().equals("HEAD") ) {/*OK*/}
            else {this.setDebugProcesso("HttpMethod é obrigatório e com valor GET ou PUT ou HEAD"); throw e;}            
        }
        if (this.getCannonicalUri()== null || this.getCannonicalUri().equals("")) {this.setDebugProcesso("CannonicalUri é obrigatório"); throw e;}  
        if (this.getAccessKey()== null || this.getAccessKey().equals("")) {this.setDebugProcesso("AccessKey é obrigatório"); throw e;}  
        if (this.getSecretKey()== null || this.getSecretKey().equals("")) {this.setDebugProcesso("SecretKey é obrigatório"); throw e;}  
        if (this.getRegionStandard()== null || this.getRegionStandard().equals("")) {this.setDebugProcesso("RegionStandard é obrigatório"); throw e;}  
        if (this.getServiceNameS3()== null || this.getServiceNameS3().equals("")) {this.setDebugProcesso("ServiceNameS3 é obrigatório"); throw e;}  
        if (this.getAws4_request()== null || this.getAws4_request().equals("")) {this.setDebugProcesso("Aws4_request é obrigatório"); throw e;}  
        if (this.getRegionStandard()== null || this.getRegionStandard().equals("")) {this.setDebugProcesso("RegionStandard é obrigatório"); throw e;}  
        if (this.getDataTrunc()== null || this.getDataTrunc().equals("")) {this.setDebugProcesso("DataTrunc é obrigatório"); throw e;} 
        else {
            try{
                SimpleDateFormat formatoData = new SimpleDateFormat("yyyyMMdd");
                Date d = formatoData.parse(this.getDataTrunc());
                //System.out.println(d.toString());
            }
            catch (Exception e1) {
                e1.printStackTrace();
                this.setDebugProcesso("DataTrunc não está no formato yyyyMMdd ou é inválida");
                throw e;
            }
            
        }
        if (this.getData()== null || this.getData().equals("")) {this.setDebugProcesso("Data é obrigatório"); throw e;} 
        else {
            try{
                SimpleDateFormat formatoData = new SimpleDateFormat("yyyyMMdd"+"HHmmss");
                String data = this.getData();
                if (data.indexOf("T")!=8) {System.out.println(data.indexOf("T"));System.out.println(data.indexOf("Z"));throw e;}
                if (data.indexOf("Z")!=15) {System.out.println(data.indexOf("Z")); throw e;}
                //System.out.println(this.getData());
                //System.out.println(this.getData().substring(0,8));
                //System.out.println(this.getData().substring(9,14));                   
                    
                Date d = formatoData.parse(this.getData().substring(0,8)+this.getData().substring(9,14));
                //System.out.println(d.toString());
            }
            catch (Exception e1) {
                e1.printStackTrace();
                this.setDebugProcesso("Data não está no formato yyyyMMddTHHmmssZ ou é inválida");
                throw e;
            }
        }
        if (this.getHeaders() == null) {this.setDebugProcesso("Headers são obrigatórios. Pelo menos:Host; X-Amz-Content-Sha256; X-Amz-Date e Authorization"); throw e;} 
        else {
            try{
                
                //System.out.println(this.getHeaders());
                Iterator<Header> headerIterator = this.getHeaders().iterator();
                
                // Validar que os seguintes headers obrigatórios estão presentes
                String reqHeaders = new String ("Host;X-Amz-Content-Sha256;X-Amz-Date;Authorization;");
                while (headerIterator.hasNext()) {
                    Header h = headerIterator.next();
                    reqHeaders = reqHeaders.replace(h.getKey()+";", "");
                    //System.out.println(h.getKey() + h.getValue());
                    //System.out.println("reqHeaders:"+reqHeaders);
                }
                //System.out.println("reqHeaders:"+reqHeaders);
                if (!reqHeaders.equals("")) throw e;
            }
            catch (Exception e1) {
                e1.printStackTrace();
                this.setDebugProcesso("Headers são obrigatórios. Pelo menos:Host; X-Amz-Content-Sha256; X-Amz-Date e Authorization");
                throw e;
            }
        }

/*        
        header.key="Host";
        header.value="";
        headers.add(header);
        header.key="X-Amz-Content-Sha256";
        header.value="";
        headers.add(header);
        header.key="X-Amz-Date";
        header.value="";
        headers.add(header);
        header.key="Authorization";
*/
    }

    public String getAuthorization ( ) {
        
        try{
        
            // Validação da informação dos headers e da informação minima necessária ao processo
            this.setDebugProcesso("");
            String returnAuth = new String ();
            

            // Validação dos argumentos do processo
            this.setDebugProcesso("Validação dos argumentos do processo");
            processValidation();
        
            
            this.setDebugProcesso("Incio Processo obtenção da Authorization S3 Rest API");

            // Obter Cannonical Request, tendo por base os headers do pedido
            // ***** colocar mais headers, n apenas os minimos olimpicos *****
            this.setDebugProcesso("Obter cannonical request.");
            String canonicalRequest = new String(
                this.getHttpMethod()+"\n"+                                        //HTTPMethod
                this.CannonicalUri+"\n"+                        //CanonicalURI
                "\n"+                                           //CanonicalQueryString
                "host:"+this.host+":"+this.port+"\n"+           //CanonicalHeaders       
                // hash payload, em principio a seguinte e do payload vazio
                "x-amz-content-sha256:"+emptyPayload+"\n"+      //CanonicalHeaders (emptyPayload)
                "x-amz-date:"+this.data+"\n"+                   //CanonicalHeaders
                "\n"+                                           
                "host;x-amz-content-sha256;x-amz-date\n"+       //Signed Headers
                emptyPayload                                    //Encoded Payload vazio
            );


            String cannonical = new String ();

            this.setDebugProcesso("Obter cannonical.");
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(canonicalRequest.getBytes(StandardCharsets.UTF_8));
            
            this.setDebugProcesso("Obter cannonical formato hex.");
            cannonical = bytesToHex(encodedhash,this.debug);

                                                                                                                                                               
            this.setDebugProcesso("Obter String para efetuar o Sign");
            String stringToSign = new String(
                AWS4HMACSHA256+"\n"+
                this.data+"\n"+
                this.dataTrunc+"/"+this.regionStandard+"/"+this.serviceNameS3+"/"+this.aws4_request+"\n"+ // scope
                cannonical
            );
            
            this.setDebugProcesso("Obter Signature Key");
            byte [] signKey = getSignatureKey (this.secretKey,this.dataTrunc,this.regionStandard, this.serviceNameS3,this.aws4_request,this.debug);

            this.setDebugProcesso("Obter Authorization key encriptada e em formato hex");
            returnAuth = bytesToHex(HmacSHA256(stringToSign,signKey),this.debug);

            // Preencher headers com base na informação
            this.setDebugProcesso("Preencher headers com base na informação");
            
            setHeadersAuthorization(returnAuth);
            
            
            return returnAuth;
        
        }
        catch (Exception e) {
            e.printStackTrace(); 
            return "";   
        }       
    }
    
    
    public static void main(String[] args) {

        // Testar processo Authorization AWS V4                
        S3AuthorizationV4  auth= new S3AuthorizationV4();
        Header header = new Header ();
        List<Header> headers = new ArrayList<Header>(); 

        /*
            String host = "172.26.33.72";
            String port = "9020";
            String CannonicalUri = "/testApiSwift";
            String accessKey = "user_pciprpc_dev";
            String secretKey = "DGbw5uHDWbLclMQSwetB8zc6e2j8wqu5kw9LLC3z";//"wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY"; //"DGbw5uHDWbLclMQSwetB8zc6e2j8wqu5kw9LLC3z";
            String regionStandard = "Standard"; //"us-east-1"; //"Standard";
            String serviceNameS3 = "s3"; //"iam";//"s3";
            String aws4_request = "aws4_request";
            String data = "20190711T183105Z";
                           20190712T023611Z
            String dataTrunc = "20190711"; //"20120215"; //"20190711";

         */

        Date data = new Date();        
        SimpleDateFormat formatoData = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatoHora = new SimpleDateFormat("HHmmss");

        auth.setHost("172.26.33.72");
        auth.setPort("9020");
        auth.setHttpMethod("HEAD");
        auth.setCannonicalUri("/bkpciprpcdev"); //+ "/"+ "iss/@pcirpc/@coimbra/3/X999/64935.pdf"); // "ALL.csv"); //iss/@pcirpc/@coimbra/3/X999/64935.pdf"); //   /iss/@pcirpc/@coimbra/3/X999/64935.pdf");
        auth.setAccessKey("user_pciprpc_dev");
        auth.setSecretKey("DGbw5uHDWbLclMQSwetB8zc6e2j8wqu5kw9LLC3z");
        auth.setRegionStandard("Standard");
        auth.setServiceNameS3("s3");
        auth.setAws4_request("aws4_request");
        auth.setData(getTimeStamp()); //"20190711T183105Z");
        auth.setDataTrunc(formatoData.format(data)); //20190711
        
        // all Http headers
        // Headers minimos necessários ao processo; como output do processo, vão ser preenchidos os respetivos valores para cada header
        header = new Header();
        header.key="Host";
        header.value="";
        headers.add(header);
        
        header = new Header();
        header.key="X-Amz-Content-Sha256";
        header.value="";
        headers.add(header);
        
        header = new Header();
        header.key="X-Amz-Date";
        header.value="";
        headers.add(header);
        
        header = new Header();
        header.key="Authorization";
        header.value="";
        headers.add(header);

        //headers adicionais
        //zero ou mais headers nececcários para a chamada S3 Rest API
        //ex: x-amz-copy-source; x-amz-metadata-directive; x-amz-meta-centrodistrital
        // para usar o processo, é necessário que todos os headers tenham o seu valor, sendo que ao não se colocar valor, assume-se que o valor do header é ""
/*
        header = new Header();
        header.key="x-amz-copy-source";
        header.value="/bkpciprpcdev/"+"iss/@pcirpc/@coimbra/3/X999/64935.pdf";    //ALL.csv"; //"iss/@pcirpc/@coimbra/3/X999/64935.pdf"; // no caso de "preencher Metadata; este valor é igual ao CannonicalUri
        headers.add(header);
        header = new Header();
        header.key="x-amz-metadata-directive";
        header.value="REPLACE";
        headers.add(header);
        header = new Header();
        header.key="x-amz-meta-centrodistrital";
        header.value="123";
        headers.add(header);
*/
        
        // Inicialização headers
        auth.setHeaders(headers);

        // debug com recurso a System.out.println 
        auth.setDebug(false);
        
        // inicialização debug do processo. em caso de sucesso, o valor manter-se-á.
        // em caso de erro, este atributo terá pistas sobre os erros que ocorreram durante o processo
        auth.setDebugProcesso("");
        
        String authorization = auth.getAuthorization();
        
        if (authorization == null || authorization.equals("")) {
            System.out.println("Erro na obtenção da Authorization para autenticação S3 Rest Api");
            System.out.println("Erros: "+auth.getDebugProcesso());
        }
        else {
                System.out.println("A Authorization S3 Rest Api é:"+authorization);
                //System.out.println(this.getHeaders());
                Iterator<Header> headerIterator = auth.getHeaders().iterator();
                
                // Validar que os seguintes headers obrigatórios estão presentes
                
                while (headerIterator.hasNext()) {
                    Header h = headerIterator.next();
                    System.out.println(h.getKey()+ ":"+ h.getValue());
                }
        }
    
    }

}


