package ecs.utilities;

public class Endpoint {
    /*
    * Oracle Consulting
    * Versão 1.0 14/07/2019
    * Ultimas Alteracoes:
    * 1.0
    * Versao inicial
    */

    String endpoint;

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }
    
    public Endpoint() {
        super();
    }
}
