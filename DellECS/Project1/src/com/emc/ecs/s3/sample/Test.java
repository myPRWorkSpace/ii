package com.emc.ecs.s3.sample;

//import com.emc.object.s3.S3Client;

import java.io.UnsupportedEncodingException;

import java.net.URISyntaxException;

import java.net.URLEncoder;

import java.nio.charset.StandardCharsets;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import java.util.Base64;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class Test {

    //private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    public static final String host = "172.26.33.72";
    public static final String port = "9020";
    public static final String CannonicalUri = "/bkpciprpcdev/iss/@pcirpc/@coimbra/3/X999/64935.pdf";
    public static final String HMAC_SHA256 = "HmacSHA256";
    public static final String accessKey = "user_pciprpc_dev";
    public static final String secretKey = "DGbw5uHDWbLclMQSwetB8zc6e2j8wqu5kw9LLC3z";//"wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY"; //"DGbw5uHDWbLclMQSwetB8zc6e2j8wqu5kw9LLC3z";
    public static final String regionStandard = "Standard"; //"us-east-1"; //"Standard";
    public static final String serviceNameS3 = "s3"; //"iam";//"s3";
    public static final String aws4_request = "aws4_request";
    public static final String data = "20190716T092311Z";
    public static final String dataTrunc = "20190716"; //"20120215"; //"20190711";
    public static final String emptyPayload = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
                                              //"e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855" -- 
    public static final String postAuthKey = "AWS4-HMAC-SHA256 Credential=user_pciprpc_dev/20190711/Standard/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=0ae4713f40961d8da7b931befae7b39b837171e8973cadf09fc33b01e16783a9";
    public static final String ENCODING = "UTF-8";
    
    public Test() {
        super();
    }

    public static String percentEncode(String s) {
        if (s == null) {
            return "";
        }
        try {
            return URLEncoder.encode(s, ENCODING)
                    // OAuth encodes some characters differently:
                    .replace("+", "%20").replace("*", "%2A")
                    .replace("%7E", "~");
            // This could be done faster with more hand-crafted code.
        } catch (UnsupportedEncodingException wow) {
            throw new RuntimeException(wow.getMessage(), wow);
        }
    }

/*
    public static String sh256 (String data, String key){
    try {
        if (key == null || data == null) throw new NullPointerException();
                        final Mac hMacSHA256 = Mac.getInstance(HMAC_SHA256);
                        byte[] hmacKeyBytes = key.getBytes(StandardCharsets.UTF_8);
                        final SecretKeySpec secretKey = new SecretKeySpec(hmacKeyBytes, HMAC_SHA256);
                        hMacSHA256.init(secretKey);
                        byte[] dataBytes = data.getBytes(StandardCharsets.UTF_8);
                        byte[] res = hMacSHA256.doFinal(dataBytes);
                        return Base64.encode(res,64);
        }
        catch (Exception e) {
            e.printStackTrace(); 
            return "";
        }       
    }  

    public static String calcShaHash (String data, String key) {
        String HMAC_SHA1_ALGORITHM = "HmacSHA1";       
        String result = null;
        System.out.println("data:"+data);
        System.out.println("key:"+key);
            

        try {         
            Key signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            result = Base64.encodeBase64String(rawHmac);    

        }
        catch (Exception e) {
            e.printStackTrace(); 
        }       

        return result;
    }
*/

/*

    static byte[] HmacSHA256(String data, byte[] key){
        try{ 
        String algorithm="HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data.getBytes("UTF-8"));
        }
        catch (Exception e) {
            e.printStackTrace(); 
            return "".getBytes();   
        }       
    }
*/
    
static byte[] HmacSHA256(String data, byte[] key) throws Exception {
    try{
    
    String algorithm="HmacSHA256";
    Mac mac = Mac.getInstance(algorithm);
    mac.init(new SecretKeySpec(key, algorithm));
    return mac.doFinal(data.getBytes("UTF-8"));

    }
    catch (Exception e) {
        e.printStackTrace(); 
        return "".getBytes();   
    }
}

    
// este metodo foi validado com os valores de https://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html
    static byte[] getSignatureKey(String key, String dateStamp) {
        try{
        
        byte[] kSecret = ("AWS4" + key).getBytes("UTF-8");
            System.out.println("keySecret:"+bytesToHex1(kSecret));
        byte[] kDate = HmacSHA256(dateStamp, kSecret);
            System.out.println("kDate:"+bytesToHex1(kDate));
        byte[] kRegion = HmacSHA256(regionStandard, kDate);
            System.out.println("kRegion:"+bytesToHex1(kRegion));
        byte[] kService = HmacSHA256(serviceNameS3, kRegion);
            System.out.println("kService:"+bytesToHex1(kService));
        byte[] kSigning = HmacSHA256(aws4_request, kService);
            System.out.println("kSigning:"+bytesToHex1(kSigning));
        return kSigning;
        }
        catch (Exception e) {
            e.printStackTrace(); 
            return "".getBytes();   
        }       
    }


    private static String bytesToHex1(byte[] hashInBytes) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashInBytes.length; i++) {
            sb.append(Integer.toString((hashInBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();

    }

    private static String bytesToHex2(byte[] hashInBytes) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashInBytes.length; i++) {
            String hex = Integer.toHexString(0xff & hashInBytes[i]);
            if (hex.length() == 1) sb.append('0');
            sb.append(hex);
        }
        return sb.toString();

    }    

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
        String hex = Integer.toHexString(0xff & hash[i]);
        if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
    
    private static String encode(String url)  
    {  
              try {  
                   String encodeURL=URLEncoder.encode( url, "UTF-8" );  
                   return encodeURL;  
              } catch (UnsupportedEncodingException e) {  
                   return "Issue while encoding" +e.getMessage();  
              }  

    }
    
    //b1f5a8f3d5ff1fa6811a5d2e143632be359e6efdeb1eb09a4c6b4dfc50365931
    //b1f5a8f3d5ff1fa6811a5d2e143632be359e6efdeb1eb09a4c6b4dfc50365931 
    
    public static void main(String[] args) {

        //LoggerFactory s = new LoggerFactory();
        
        Date date = new Date();
        long millis = date.getTime()+1000;
        System.out.println(date.toString());
        date = new Date(millis);
        System.out.println("mil:"+millis);
                    

    String uri = "/bkpciprpc_dev";
    System.out.println("encode:"+percentEncode(uri));

/*
    String awsCannonicalRequest = new String (
        "GET\n"+
        "/test.txt\n"+
        "\n"+
        "host:examplebucket.s3.amazonaws.com\n"+
        "range:bytes=0-9\n"+
        "x-amz-content-sha256:"+hashRequestBody+"\n"+
        "x-amz-date:20130524T000000Z\n"+
        "\n"+
        "host;range;x-amz-content-sha256;x-amz-date\n"+
        hashRequestBody);

        String awsCannonical = new String ();
        try{
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(awsCannonicalRequest.getBytes(StandardCharsets.UTF_8));
        
        awsCannonical = bytesToHex(encodedhash);
            
            
        } catch (Exception e){}

        System.out.println("Valores:"+"\n");
        System.out.println(awsCannonicalRequest+"\n");
        System.out.println("Valores2:"+"\n");
        System.out.println(awsCannonical+"\n");
        System.out.println("7344ae5b7ee6c3e7e6b0fe0640412a37625d1fbfff95c48bbb2dc43964946972\n");
*/

/*
CanonicalRequest =
  HTTPRequestMethod + '\n' +
  CanonicalURI + '\n' +
  CanonicalQueryString + '\n' +
  CanonicalHeaders + '\n' +
  SignedHeaders + '\n' +
  HexEncode(Hash(RequestPayload))
*/
        String canonicalRequest = new String(
        "GET\n"+                              //HTTPMethod
        // ??? Com ou sem / inicial
        percentEncode(CannonicalUri)+"\n"+         //CanonicalURI
        "\n"+                                 //CanonicalQueryString
        "host:"+host+":"+port+"\n"+                              //CanonicalHeaders       
        // hash payload, em principio a seguinte e do payload vazio (gerado no postman)         
        "x-amz-content-sha256:"+emptyPayload+"\n"+   //CanonicalHeaders (emptyPayload)
        "x-amz-date:"+data+"\n"+                        //CanonicalHeaders
        "\n"+
        "host;x-amz-content-sha256;x-amz-date\n"+
        emptyPayload
        );


        //4ff2316a9aa3b2cf2bc007a07fa381756db1605cce526372c263d81cecfe0395
        //b1f5a8f3d5ff1fa6811a5d2e143632be359e6efdeb1eb09a4c6b4dfc50365931
              
        String cannonical = new String ();
        try{
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(canonicalRequest.getBytes(StandardCharsets.UTF_8));
        cannonical = bytesToHex(encodedhash);
        } catch (Exception e){}


        System.out.println("Valores II:"+"\n");
        System.out.println(canonicalRequest+"\n");
        System.out.println(cannonical+"\n");


  
/*
        String awsStringToSign = new String(
            "AWS4-HMAC-SHA256\n"+
            "20130524T000000Z\n"+
            "20130524/us-east-1/s3/aws4_request\n"+ // scope
            awsCannonical
            );
            
            System.out.println("AwStrig:"+awsStringToSign+"\n");
*/ 
        //AWS4-HMAC-SHA256 Credential=
        //user_pciprpc_dev/20190711/Standard/s3/aws4_request, 
        //SignedHeaders=host;x-amz-content-sha256;x-amz-date, 
        //Signature=4ff2316a9aa3b2cf2bc007a07fa381756db1605cce526372c263d81cecfe0395
                                                                                                                                                            
        String stringToSign = new String(
        "AWS4-HMAC-SHA256\n"+
        data+"\n"+
        dataTrunc+"/"+regionStandard+"/"+serviceNameS3+"/"+aws4_request+"\n"+ // scope
        cannonical
        );
        System.out.println("stringToSign:"+stringToSign+"\n");
        
//        AWS4-HMAC-SHA256
//        20150830T123600Z
//        20150830/us-east-1/iam/aws4_request
//        f536975d06c0309214f805bb90ccff089219ecd68b2577efef23edd43b7e1a59
            


//    String awsKey ="wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY";
            System.out.println("------------------------");
            
            try{

//        byte [] awsSignKey = HmacSHA256("aws4_request",HmacSHA256("s3",HmacSHA256("us-east-1",HmacSHA256("20130524",("AWS4"+ awsKey).getBytes("UTF-8")))));
        //byte [] signKey = HmacSHA256(aws4_request,HmacSHA256(serviceNameS3,HmacSHA256(regionStandard,HmacSHA256(dataTrunc,("AWS4"+ secretKey).getBytes("UTF-8")))));
        byte [] signKey = getSignatureKey (secretKey,dataTrunc);
        System.out.println(signKey + "="+signKey);

/* Validar ...
            static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception {
                byte[] kSecret = ("AWS4" + key).getBytes("UTF-8");
                byte[] kDate = HmacSHA256(dateStamp, kSecret);
                byte[] kRegion = HmacSHA256(regionName, kDate);
                byte[] kService = HmacSHA256(serviceName, kRegion);
                byte[] kSigning = HmacSHA256("aws4_request", kService);
                return kSigning;
            }
*/
        
                    
            System.out.println("------------------------");

//            String awsSignature = bytesToHex1(HmacSHA256(awsStringToSign,awsSignKey));
            String signature = bytesToHex1(HmacSHA256(stringToSign,signKey));
            System.out.println("+++++++++++++++++++++++++");
            System.out.println("stringToSign-->"+stringToSign+"\n");
        
//            System.out.println("awsSignature-->"+awsSignature+"\n");
            System.out.println("Signature-->"+signature+"\n");
                
            System.out.println("+++++++++++++++++++++++++");

            String newAuthKey="AWS4-HMAC-SHA256 Credential="+accessKey+"/"+dataTrunc+ "/"+regionStandard+"/"+serviceNameS3+"/"+aws4_request+", SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature="+signature;
            System.out.println(newAuthKey);
            //System.out.println(postAuthKey);    

            // POSTMAN
            //AWS4-HMAC-SHA256 Credential=user_pciprpc_dev/20190711/Standard/s3/aws4_request, 
            //SignedHeaders=host;x-amz-content-sha256;x-amz-date, 
            //Signature=919c320f566abee4ca3d02f1bdf74bceb1841252897c5a8663bb2ca0498df523


        } catch (Exception e) {}

    }
        
        
    
}
